package dominio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Es la clase que permite trabajar con el Catalogo de Bicicletas
 * @author Victor
 *
 */
public class Catalogo{

	/**
	 *  coleccionBicis:  Coleccion de bicis para trabajar en el programa
	 *  nombreFichero:   Fichero que guarda la coleccion de bicis 
	 */
    private ArrayList<Bici> coleccionBicis = new ArrayList<>();
    private String nombreFichero = "bicis.txt";

    /**
     *  Carga la coleccion de bicis en el fichero 
     */
    public Catalogo(){
        cargarBicis();
    }

    /**
     * Agrega bicicletas
     * @param bici
     */
    public void annadirBicis(Bici bici){
    	coleccionBicis.add(bici);
        volcarBicis();
    }

    /**
     * Modifica las bicicletas 
     * @param marca: La marca de la bicicleta a modificar
     * @param bici:  Los nuevos valores que tendrá la bicicleta
     * 
     */
    public void annadirBicis(String marca, Bici bici){
    	eliminarBicis(marca);
    	coleccionBicis.add(bici);
        volcarBicis();
    }
    
    /**
     * Elimina las bicicletas
     * @param marca:  La marca de la bicicleta a eliminar
     */
    public void eliminarBicis(String marca){
    	// La variable borrar porque no se puede borrar dentro del ciclo 
    	// y el break para que se salga del ciclo cuando la encuentre
    	Bici borrar=new Bici();
    	for (Bici bici : coleccionBicis) {
			if (bici.getMarca().equals(marca)) {
				borrar=bici;
				break;
			}
		}
    	coleccionBicis.remove(borrar);
        volcarBicis();
    }
    
   
    
    
    @Override
    /**
     *  Sobreescritura del metodo toString para mostrar todas las bicicletas
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (Bici bici : coleccionBicis) sb.append(bici + "\n");
        return sb.toString();
    }

    /**
     * Vuelca las bicicletas en el fichero bicis.txt
     */
    private void volcarBicis(){
        try{
            FileWriter fw = new FileWriter(nombreFichero);
            for(Bici bici : coleccionBicis){
                fw.write(bici.getNombre() + " " + 
                        bici.getModelo()  + " " +
                		bici.getNroRuedas() + " " +
                        bici.getMarca()+ " " +
                		bici.getPeso()+ "\n");
            }
            fw.close();
        }catch(IOException e){
            System.out.println("Ha habido un problema al intentar escribir en el fichero " + nombreFichero); 
        }
    }

    /**
     *  Carga las bicicletas del fichero a coleccionBicis
     *  Con coleccionBicis se trabaja en el programa
     *  
     */
    private void cargarBicis(){
        try{
            File fichero = new File(nombreFichero);
            if (fichero.createNewFile()) System.out.println("Se ha creado el fichero " + nombreFichero); 
            Scanner sc = new Scanner(fichero);
            while(sc.hasNext()){
                coleccionBicis.add(new Bici(sc.next(), sc.next(), sc.next(),sc.next(),sc.next()));
            }
        }catch(IOException e){
            System.out.println("Ha habido un problema al intentar leer en el fichero " + nombreFichero);
            System.out.println(e); 
        }
    }
}
