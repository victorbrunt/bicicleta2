package dominio;

public class Vehiculo{
    private String nombre;
    private String modelo;
    private Integer nroRuedas;

    public Vehiculo(String nombre, String modelo, String ruedas) {
        this.nombre = nombre;
        this.modelo = modelo;
        this.nroRuedas = Integer.parseInt(ruedas);
    }

    public Vehiculo() {
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setmodelo(String modelo) {
        this.modelo = modelo;
    }

    public String getModelo() {
        return modelo;
        
    }
    
    public void setNroRuedas(Integer numero) {
        this.nroRuedas=numero;
    }

    public Integer getNroRuedas() {
        return nroRuedas;
    }

    @Override
    public String toString() {
        return getNombre() + ": " + getModelo()+ " "+getNroRuedas();
    }

}
