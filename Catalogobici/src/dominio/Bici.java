package dominio;
/**
*    Bici hereda de Vehiculo
*    Bici es la clase que tiene el manejo de las bicicletas
*
**/
public class Bici extends Vehiculo{
    private String marca;
    private Double peso;

    public Bici(String nombre, String modelo, String ruedas, String marca, String peso) {
        super(nombre, modelo, ruedas);
        this.peso=Double.parseDouble(peso);
        this.marca = marca;
    }

    public Bici() {
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getMarca() {
        return marca;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public Double getPeso() {
        return peso;
    }

    @Override
    public String toString() {
        return super.toString()+" Marca: "+ getMarca() + " peso: "+getPeso() + " libras";
    }

}
