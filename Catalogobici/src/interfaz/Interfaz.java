package interfaz;

import dominio.*;

/**
*  Representa la interfaz con el usuario
*
**/
public class Interfaz{
    public static void mostrarAyuda(){
        System.out.println("Pendiente de programar."); 
    }
    public static void ejecutar(String[] parametros){
        try{
            String instruccion = parametros[0];
            Catalogo catalogo = new Catalogo();
            switch(instruccion){
                // agrega la bicicleta 
                case "add":
                	catalogo.annadirBicis(new Bici(parametros[1], parametros[2], parametros[3], parametros[4], parametros[5]));
                    System.out.println("Bicicleta añadida");
                    break;
		// modifica la bicicleta
                case "chg":
                	catalogo.annadirBicis(parametros[1], new Bici(parametros[2], parametros[3], parametros[4], parametros[5], parametros[6]));
                    System.out.println("Bicicleta cambiada");
                    break;    
		// remueve la bicicleta
                case "rem":
                	catalogo.eliminarBicis(parametros[1]);
                    System.out.println("Bicicleta removida");
                    break;    
		// lista las bicicletas
                case "list":
                    System.out.println(catalogo);
                    break;
		// Ayuda
                case "--help": case "-h":
                    mostrarAyuda();
                    break;
                default:
                    System.out.println("Opción incorrecta");
                    mostrarAyuda();    

            }
        }catch(java.lang.ArrayIndexOutOfBoundsException e){
            System.out.println("Opción incorrecta");
            mostrarAyuda(); 
        } 
    }
}
